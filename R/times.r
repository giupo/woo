#' questa funzione esegue un prodotto
#'
#' @name times
#' @param x first term of product
#' @param y second term of product
#' @return x * y
#' @export

times <- function(x, y) {
  x * y
}
